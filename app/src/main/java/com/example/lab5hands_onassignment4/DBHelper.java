package com.example.lab5hands_onassignment4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Test";
    SQLiteDatabase database ;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        database=db;
        db.execSQL("create table department (DeptId integer primary key autoincrement , name text)" );
        db.execSQL("create table employee (EmpId integer primary key autoincrement , name text not null,title text not null,phone text not null, email text not null,dept_id integer ,Foreign key(dept_id) References department (DeptId))" );
        //inser Dummy emp
      createNewEmp("dummy","Ahmed","dummy",123,"dummy" ,false);
      createNewEmp("dummy","Mohamed","dummy",123,"dummy" ,false);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("drop table if exists employee");
        db.execSQL("drop table if exists department");
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public void createNewEmp(String deptName ,String  empName ,String title ,int phone  , String email,boolean openDd  ){
        ContentValues row = new ContentValues();
        row.put("name",deptName);
        if(openDd){
            database = getWritableDatabase();
        }
        database.insert("department",null,row);
        row = new ContentValues();
        row.put("name",empName);
        row.put("title",title);
        row.put("phone",phone);
        row.put("email",email);
        database.insert("employee",null,row);
        if(openDd){
            database.close();
        }
    }
    public ArrayList<String> fetchAllEmpNameWhere (String arg){
        Log.d("searchRes",arg);
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select name from employee where name like ? ",new String[] { "%" + arg + "%" });
        ArrayList<String> returnedList = new ArrayList<>();
        if(cursor!=null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                returnedList.add(cursor.getString(0));
                Log.d("searchRes","Cursoe isn't null");
                cursor.moveToNext();
            }
        }
        else{
            Log.d("searchRes","Cursoe is null");
        }
        database.close();
        return returnedList;
    }
}
