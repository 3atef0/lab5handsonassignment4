package com.example.lab5hands_onassignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button buttonSearch;
    Button buttonAdd;
    EditText editTextSearch ;
    ListView listView_SearchRes ;
    ArrayList<String> searchRes ;
    DBHelper dbHelper;
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonSearch = findViewById(R.id.butt_search);
        buttonAdd = findViewById(R.id.butt_add);
        editTextSearch = findViewById(R.id.eT_Search);
        listView_SearchRes = findViewById(R.id.listView_SearchRes);
        searchRes = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, android.R.id.text1,searchRes);
        listView_SearchRes.setAdapter(arrayAdapter);
        dbHelper =new DBHelper(this);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextSearch.getText().toString().isEmpty()){
                    clearList();
                }else{
                   ArrayList<String> arrayList =  dbHelper.fetchAllEmpNameWhere(editTextSearch.getText().toString())    ;
                    searchRes.clear();
                    searchRes.addAll(arrayList);
                    arrayAdapter.notifyDataSetChanged();
                }
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextSearch.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "INVALID", Toast.LENGTH_SHORT).show();
                }else{
                    dbHelper.createNewEmp("dummy",editTextSearch.getText().toString(),"dummy",123,"dummy" ,true);
                    clearList();
                }
            }
        });
    }
    public void clearList(){
        searchRes.clear();
        arrayAdapter.notifyDataSetChanged();
    }
}
